package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import static org.junit.Assert.*;

public class StorageTest {

    private final ByteArrayOutputStream outputStreamContent = new ByteArrayOutputStream();
    private final PrintStream originalOutputStream = System.out;

    @Before
    public void setUpOutputStream() {
        System.setOut(new PrintStream(outputStreamContent));
    }

    @After
    public void restoreOutputStream() {
        System.setOut(originalOutputStream);
    }

    private void clearOutputStreamContent() {
        outputStreamContent.reset();
    }

    @Test
    public void testPrintListOfStoredItems() {
        Storage storage = new Storage();

        // nejprve zkusim, jak bude vypadat formatovany vypis prvku, kdyz tam zadne nejsou
        String expectedOutput = "STORAGE IS CURRENTLY CONTAINING:\n";
        storage.printListOfStoredItems();
        assertEquals(expectedOutput, outputStreamContent.toString());

        // vlozim nejake prvky
        Item testItem1 = new Item(10,"kytka",14.90f,"botanika") {};
        Item testItem2 = new Item(11,"plevel",12.90f,"botanika") {};

        storage.insertItems(testItem1, 5);
        storage.insertItems(testItem2, 8);

        // vycistim si stream
        this.clearOutputStreamContent();

        // pripravim si ocekavany vystup vypisu
        String stockOfitemTemplate = (
                "STOCK OF ITEM:  Item   ID %s   NAME %s   CATEGORY botanika    PIECES IN STORAGE: %s\n"
        );
        expectedOutput += String.format(stockOfitemTemplate, 10, "kytka", 5);
        expectedOutput += String.format(stockOfitemTemplate, 11, "plevel", 8);

        // otestuji, jak bude vypadat formatovany vypis prvku s vlozenymi prvky
        storage.printListOfStoredItems();
        assertEquals(expectedOutput, outputStreamContent.toString());
    }

    @Test
    public void testGetStockEntries_noEntries_shouldReturnEmptyCollection() {
        Storage storage = new Storage();
        assertTrue(storage.getStockEntries().isEmpty());
    }

    @Test
    public void testGetStockEntries_insertNewEntry_shouldReturnListWithOneEntry() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(item.getID(), it.next().getItem().getID());
        assertFalse(it.hasNext());
    }

    @Test
    public void testGetStockEntries_insertSameEntry_shouldIncreaseCount() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(2, it.next().getCount());
    }

    @Test(expected = NoItemInStorage.class)
    public void testRemoveItems_noItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.removeItems(item, 10);
    }

    @Test(expected = NoItemInStorage.class)
    public void testRemoveItems_insertOneItem_removeTwoItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);

        storage.removeItems(item, 2);
    }

    @Test
    public void testRemoveItems_insertItem_removeItem_shouldDecreaseCount() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);
        storage.removeItems(item, 1);

        assertEquals(0, storage.getStockEntries().iterator().next().getCount());
    }

    @Test
    public void testProcessOrder_emptyShoppingCard_shouldJustRun() throws NoItemInStorage {
        Order order = new Order(new ShoppingCart());
        new Storage().processOrder(order);
    }

    @Test(expected = NoItemInStorage.class)
    public void testProcessOrder_itemNotInStorage_shouldThrowException() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "apple", 1f, "food", 15));
        Order order = new Order(cart);
        new Storage().processOrder(order);
    }

    @Test
    public void testGetItemCount_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem));
    }

    @Test
    public void testGetItemCount_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item));
    }

    @Test
    public void testGetItemCount_byId_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem.getID()));
    }

    @Test
    public void testGetItemCount_byId_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item.getID()));
    }

    @Test
    public void testGetPriceOfWholeStock_emptyStack_shouldReturnZero() {
        Storage storage = new Storage();

        assertEquals(0, storage.getPriceOfWholeStock());
    }

    @Test
    public void testGetPriceOfWholeStock_nonEmptyStack_shouldReturnSumOfPrices() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 55F, "food", 15);

        storage.insertItems(item, 1);

        assertEquals(55F, storage.getPriceOfWholeStock(), 0);
    }

    @Test
    public void testGetItemsOfCategorySortedByPrice_unknownCategory_shouldReturnEmptyColection() {
        Storage storage = new Storage();
        assertTrue(storage.getItemsOfCategorySortedByPrice("unknown").isEmpty());

    }

    /**
     * Vytvoří seřazený a neseřazený Storage se stejnými Item, otestuje jestli
     * metoda getItemsOfCategorySortedByPrice na neseřazeném Storage
     * vrátí stejný výsledek, jako metoda getItemsByCategory pro seřazený Storage
     */
    @Test
    public void testsortItemsByPrice() {
        Storage store = new Storage();
        Storage store2 = new Storage();
        //unsorted
        store.insertItems(new Item(13,"kytka3",13.90f,"botanika"){},2);
        store.insertItems(new Item(12,"kytka2",12.90f,"botanika"){},2);
        store.insertItems(new Item(11,"kytka1",11.90f,"botanika"){},2);
        store.insertItems(new Item(14,"kytka4",14.90f,"botanika"){},2);
        //sorted
        store2.insertItems(new Item(11,"kytka1",11.90f,"botanika"){},2);
        store2.insertItems(new Item(12,"kytka2",12.90f,"botanika"){},2);
        store2.insertItems(new Item(13,"kytka3",13.90f,"botanika"){},2);
        store2.insertItems(new Item(14,"kytka4",14.90f,"botanika"){},2);
        assertEquals(store2.getItemsByCategory("botanika"), store.getItemsOfCategorySortedByPrice("botanika"));
    }

    /**
     * Testuje ItemComparator, který porovnává Itemy podle ceny
     */
    @Test
    public void testItemComparator() {
        Item item = new Item(11,"kytka1",11.90f,"botanika"){};
        Item item2 = new Item(12,"kytka2",12.90f,"botanika"){};
        ItemComparator comparator = new ItemComparator();
        //item je levnější nebo stejně drahý jako item2
        boolean known_true = item.getPrice()<item2.getPrice();
        boolean unknown_true = comparator.compare(item,item2)==-1;
        assertEquals(known_true,unknown_true);
        //item je dražší než item2 (pouze otočená nerovnost)
        boolean known_false = item.getPrice()>item2.getPrice();
        boolean unknown_false = comparator.compare(item2,item)==-1;
        assertEquals(known_false,unknown_false);
    }

}