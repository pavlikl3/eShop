package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemStockTest {

    /**
     * Otestuje zvyseni promenne count.
     * Testuje primo promennou count, ne metodu getCount (ta je otestovana ve vlastnim testu).
     */
    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        // otestuji ze je tam na zacatku 0
        assertEquals(0, itemStock.count);

        // pridam kladny pocet
        itemStock.IncreaseItemCount(10);
        assertEquals(10, itemStock.count);

        // pridam zaporny pocet
        itemStock.IncreaseItemCount(-5);
        assertEquals(5, itemStock.count);
    }

    /**
     * Otestuje snizeni promenne count.
     * Testuje primo promennou count, ne metodu getCount (ta je otestovana ve vlastnim testu).
     */
    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        // otestuji ze je tam na zacatku 0
        assertEquals(0, itemStock.count);

        // odeberu kladny pocet
        itemStock.decreaseItemCount(10);
        assertEquals(-10, itemStock.count);

        // odeberu zaporny pocet
        itemStock.decreaseItemCount(-5);
        assertEquals(-5, itemStock.count);
    }

    /**
     * Otestuje ziskani promenne count.
     */
    @Test
    public void getCount() {
        ItemStock itemStock = new ItemStock(null);

        // test inicializace na 0
        assertEquals(0, itemStock.getCount());

        // prime prirazeni cisla do promenne
        itemStock.count = -666;
        assertEquals(-666, itemStock.getCount());
        itemStock.count = 666;
        assertEquals(666, itemStock.getCount());
    }

    /**
     * Otestuje getItem
     * Přidá Item do ItemStock následně a porovná přímou referenci
     * s výstupem metody getItem
     */
    @Test
    public void testGetItem() {
        ItemStock itemStocktest = new ItemStock(new Item(10,"kytka",14.90f,"botanika") {
        });
        Item testItem = itemStocktest.getItem();
        assertEquals(itemStocktest.refItem, testItem);
    }

    /**
     * Otestuje, ze metoda testToString vraci ocekavany vystup (formatovany vypis nazvu tridy a jejich atributu).
     */
    @Test
    public void testToString() {
        ItemStock itemStocktest = new ItemStock(new Item(10,"kytka",14.90f,"botanika") {
        });
        String expectedOutput = "STOCK OF ITEM:  Item   ID 10   NAME kytka   CATEGORY botanika    PIECES IN STORAGE: 0";
        assertEquals(expectedOutput, itemStocktest.toString());
    }
}